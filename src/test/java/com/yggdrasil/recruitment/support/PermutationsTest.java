package com.yggdrasil.recruitment.support;

import org.junit.Test;

import java.util.*;
import java.util.stream.Collectors;

import static org.assertj.core.api.Assertions.*;



public class PermutationsTest {

    @Test
    public void of_zero_elements_should_return_one_empty_list() {

        List<String> strings = Collections.emptyList();

        List<List<String>> permutations = Permutations.of(strings).collect(Collectors.toList());

        assertThat(permutations).hasSize(1);
        assertThat(permutations.get(0)).hasSize(0);
    }

    @Test
    public void of_one_element_should_return_one_list_with_one_element() {

        List<String> strings = Collections.singletonList("A");

        List<List<String>> permutations = Permutations.of(strings).collect(Collectors.toList());

        assertThat(permutations).hasSize(1);
        assertThat(permutations.get(0)).hasSize(1);
        assertThat(permutations.get(0)).containsOnly("A");
    }

    @Test
    public void of_four_elements_should_return_24_different_lists_with_four_element() {

        List<String> strings = Arrays.asList("A", "B", "C", "D");

        List<List<String>> permutations = Permutations.of(strings).collect(Collectors.toList());

        assertThat(permutations).hasSize(24); // factorial of 4
        permutations.forEach(stringList -> {
            assertThat(stringList).hasSize(4);
            assertThat(stringList).containsExactlyInAnyOrder("A", "B", "C", "D");
        });
        Set<List<String>> set = new HashSet<>(permutations);
        assertThat(set).hasSize(24);
    }
}