package com.yggdrasil.recruitment.support;

import org.junit.Before;
import org.junit.Test;

import java.math.BigDecimal;
import java.util.stream.IntStream;

import static org.assertj.core.api.Assertions.assertThat;

public class SumAndCountTest {

    SumAndCount underTest;
    @Before
    public void setUp() {
        underTest = new SumAndCount();
    }

    @Test
    public void initializationTest_initialized_element_is_set_to_zero() {
        assertThat(underTest.average()).isEqualTo("0");
    }

    @Test
    public void return_average_of_added_elements() {
        IntStream.rangeClosed(0, 100).forEach(i -> underTest.add(new BigDecimal(i)));
        BigDecimal expected = new BigDecimal((100 - 0)*50.5/101);
        assertThat(underTest.average()).isEqualByComparingTo(expected);
    }

}