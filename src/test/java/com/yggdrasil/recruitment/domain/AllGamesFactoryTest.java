package com.yggdrasil.recruitment.domain;

import com.yggdrasil.recruitment.config.GameConfiguration;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

import static org.assertj.core.api.Assertions.assertThat;

@RunWith(MockitoJUnitRunner.class)
public class AllGamesFactoryTest {

    AllGamesFactory underTest;

    @Mock
    GameConfiguration testConfig;

    @Before
    public void setup() {
        underTest = new AllGamesFactory(testConfig);
    }


    @Test
    public void testZeroMainPhaseRewards() {
        //given
        Mockito.when(testConfig.mainPhaseRewards()).thenReturn(Collections.emptyList());

        //when
        List<Game> allGames = underTest.allGames().collect(Collectors.toList());

        //then
        assertThat(allGames).hasSize(1);
        assertThat(allGames.get(0).play()).isEqualTo("0");

    }

    @Test
    public void testOneMainPhaseRewards() {
        //given
        Mockito.when(testConfig.mainPhaseRewards()).thenReturn(Collections.singletonList(Reward.EURO_10));

        //when
        List<Game> allGames = underTest.allGames().collect(Collectors.toList());

        //then
        assertThat(allGames).hasSize(1);
        assertThat(allGames.get(0).play()).isEqualTo("10");

    }

    @Test
    public void testFourMainPhaseRewards() {
        //given
        List<Reward> rewards = Arrays.asList(Reward.EURO_10, Reward.EURO_100, Reward.EURO_5, Reward.GAME_OVER);
        Mockito.when(testConfig.mainPhaseRewards()).thenReturn(rewards);

        //when
        List<Game> allGames = underTest.allGames().collect(Collectors.toList());

        //then
        assertThat(allGames).hasSize(24); // factorial of 4
    }

}