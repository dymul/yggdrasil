package com.yggdrasil.recruitment.domain;

import org.junit.Test;

import java.util.Collections;
import java.util.Optional;

import static org.assertj.core.api.Java6Assertions.assertThat;

public class GameWithSecondChanceTest {

    Game underTest;

    @Test
    public void secondChanceGame_returns_notEmpty_second_chance_game() {
        //given
        Game secondChanceGame = new Game(Collections.emptyList(), Reward.EMPTY);
        underTest = new GameWithSecondChance(Collections.emptyList(), Reward.EMPTY, secondChanceGame);

        //when
        Optional<Game> game = underTest.secondChanceGame();

        //then
        assertThat(game.isPresent()).isTrue();
        assertThat(game.get()).isEqualTo(secondChanceGame);

    }

    @Test(expected = IllegalArgumentException.class)
    public void secondChanceGame_cannot_be_created_with_null_scond_chance_game() {
        //given
        underTest = new GameWithSecondChance(Collections.emptyList(), Reward.EMPTY, null);
    }
}