package com.yggdrasil.recruitment.domain;

import com.yggdrasil.recruitment.config.EmbeddedGameConfiguration;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.Optional;
import java.util.Random;

import static org.assertj.core.api.Java6Assertions.assertThat;


@RunWith(MockitoJUnitRunner.class)
public class RandomGameFactoryTest {

    RandomGameFactory underTest;

    @Mock
    Random random;

    @Before
    public void setUp() {
        underTest = new RandomGameFactory(random, new EmbeddedGameConfiguration());
    }

    @Test
    public void randomGameStream_return_games_without_second_chance_if_random_return_less_than_three() {
        //given
        Mockito.when(random.nextInt(4)).thenReturn(2);

        Optional<Game> game = underTest.randomGameStream().limit(1000).filter(g -> g instanceof GameWithSecondChance).findFirst();

        assertThat(game.isPresent()).isFalse();
    }

    @Test
    public void randomGameStream_return_games_with_second_chance_if_random_return_3() {
        //given
        Mockito.when(random.nextInt(4)).thenReturn(3);

        Optional<Game> game = underTest.randomGameStream().limit(1000).filter(g -> !(g instanceof GameWithSecondChance)).findFirst();

        assertThat(game.isPresent()).isFalse();
    }

    @Test
    public void randomGameStream_second_chance_game_never_gives_second_chance() {
        //given
        Mockito.when(random.nextInt(4)).thenReturn(3);

        Optional<Game> game = underTest.randomGameStream().limit(1000)
                .filter(g -> g.secondChanceGame().get() instanceof GameWithSecondChance)
                .findFirst();

        assertThat(game.isPresent()).isFalse();
    }
}