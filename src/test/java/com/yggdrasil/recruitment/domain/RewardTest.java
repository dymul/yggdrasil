package com.yggdrasil.recruitment.domain;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.math.BigDecimal;
import java.util.Collections;
import java.util.Optional;

import static org.assertj.core.api.Java6Assertions.assertThat;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyZeroInteractions;
import static org.mockito.Mockito.when;


@RunWith(MockitoJUnitRunner.class)
public class RewardTest {

    @Mock
    Game game;

    @Test
    public void getPayoff() {
        assertThat(Reward.EURO_100.getPayoff()).isEqualTo("100");
        assertThat(Reward.EURO_20.getPayoff()).isEqualTo("20");
        assertThat(Reward.EURO_10.getPayoff()).isEqualTo("10");
        assertThat(Reward.EURO_5.getPayoff()).isEqualTo("5");
        assertThat(Reward.EXTRA_LIFE.getPayoff()).isEqualTo("0");
        assertThat(Reward.GAME_OVER.getPayoff()).isEqualTo("0");
        assertThat(Reward.SECOND_CHANCE.getPayoff()).isEqualTo("0");
        assertThat(Reward.EMPTY.getPayoff()).isEqualTo("0");

    }

    @Test
    public void addRewardTo_for_100() {
        Reward.EURO_100.addRewardTo(game);
        verify(game).increasePayoff(Reward.EURO_100.getPayoff());
    }

    @Test
    public void addRewardTo_for_20() {
        Reward.EURO_20.addRewardTo(game);
        verify(game).increasePayoff(Reward.EURO_20.getPayoff());
    }

    @Test
    public void addRewardTo_for_10() {
        Reward.EURO_10.addRewardTo(game);
        verify(game).increasePayoff(Reward.EURO_10.getPayoff());
    }

    @Test
    public void addRewardTo_for_5() {
        Reward.EURO_5.addRewardTo(game);
        verify(game).increasePayoff(Reward.EURO_5.getPayoff());
    }

    @Test
    public void addRewardTo_for_extraLife() {
        Reward.EXTRA_LIFE.addRewardTo(game);
        verify(game).extraLife();
    }

    @Test
    public void addRewardTo_for_gameOver() {
        Reward.GAME_OVER.addRewardTo(game);
        verify(game).gameOver();
    }

    @Test
    public void addRewardTo_for_empty() {
        Reward.EMPTY.addRewardTo(game);
        verifyZeroInteractions(game);
    }

    @Test
    public void addRewardTo_for_secondChance() {
        when(game.secondChanceGame()).thenReturn(Optional.of(new Game(Collections.emptyList(), Reward.EMPTY)));
        Reward.SECOND_CHANCE.addRewardTo(game);
        verify(game).increasePayoff(BigDecimal.ZERO);
    }

}