package com.yggdrasil.recruitment.domain;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.mockito.stubbing.Answer;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class GameTest {

    Game underTest;

    @Mock Reward box1;
    @Mock Reward box2;
    @Mock Reward box3;
    @Mock Reward box4;
    @Mock Reward box5;
    @Mock Reward additionalReward;

    @Before
    public void setUp() {
        List<Reward> boxes = Arrays.asList(box1, box2, box3, box4, box5);
        underTest = new Game(boxes, additionalReward);
    }

    @Test
    public void play_will_call_addReward_on_all_boxes() {
        //when
        underTest.play();

        //then
        verify(box1).addRewardTo(underTest);
        verify(box2).addRewardTo(underTest);
        verify(box3).addRewardTo(underTest);
        verify(box4).addRewardTo(underTest);
        verify(box5).addRewardTo(underTest);
        verify(additionalReward).addRewardTo(underTest);
    }

    @Test
    public void play_will_call_addReward_on_all_boxes_until_gameOver() {

        //given
        doAnswer(game_over).when(box2).addRewardTo(underTest);

        //when
        underTest.play();

        //then
        verify(box1).addRewardTo(underTest);
        verify(box2).addRewardTo(underTest);
        verifyZeroInteractions(box3);
        verifyZeroInteractions(box4);
        verifyZeroInteractions(box5);
        verify(additionalReward).addRewardTo(underTest);
    }

    @Test
    public void play_will_call_addReward_on_all_boxes_until_second_gameOver_when_has_extra_life() {

        //given
        doAnswer(extraLife).when(box2).addRewardTo(underTest);
        doAnswer(game_over).when(box3).addRewardTo(underTest);
        doAnswer(game_over).when(box4).addRewardTo(underTest);

        //when
        underTest.play();

        //then
        verify(box1).addRewardTo(underTest);
        verify(box2).addRewardTo(underTest);
        verifyZeroInteractions(box5);
        verify(additionalReward).addRewardTo(underTest);
    }

    @Test
    public void play_will_call_addReward_on_all_boxes_before_first_game_over_if_extraLife_later() {

        //given
        doAnswer(game_over).when(box3).addRewardTo(underTest);
        doAnswer(extraLife).when(box4).addRewardTo(underTest);

        //when
        underTest.play();

        //then
        verify(box1).addRewardTo(underTest);
        verify(box2).addRewardTo(underTest);
        verifyZeroInteractions(box4);
        verifyZeroInteractions(box5);
        verify(additionalReward).addRewardTo(underTest);
    }

    @Test
    public void increasePayOff_increases_payOff() {
        underTest = new Game(Collections.emptyList(), Reward.EMPTY);

        BigDecimal payoff = underTest.play();

        assertThat(payoff).isEqualTo("0");

        underTest.increasePayoff(BigDecimal.ONE);

        payoff = underTest.play();

        assertThat(payoff).isEqualTo("1");

    }

    Answer<Void> game_over = invocation -> {
        Game game = invocation.getArgumentAt(0, Game.class);
        game.gameOver();
        return null;
    };

    Answer<Void> extraLife = invocation -> {
        Game game = invocation.getArgumentAt(0, Game.class);
        game.extraLife();
        return null;
    };

    @Test
    public void secondChanceGame() {
        //when
        Optional<Game> secondChanceGame = underTest.secondChanceGame();

        //then
        assertThat(secondChanceGame.isPresent()).isFalse();

    }
}