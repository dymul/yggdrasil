package com.yggdrasil.recruitment;

import com.yggdrasil.recruitment.config.EmbeddedGameConfiguration;
import com.yggdrasil.recruitment.domain.Game;
import com.yggdrasil.recruitment.domain.RandomGameFactory;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Random;

public class Simulation {

    private static final int NUMBER_OF_ROUNDS = 10000000;

    public static void main(String[] args) {

        RandomGameFactory factory = new RandomGameFactory(new Random(), new EmbeddedGameConfiguration());
        BigDecimal result = factory.randomGameStream()
                .limit(NUMBER_OF_ROUNDS)
                .map(Game::play)
                .reduce(BigDecimal.ZERO, BigDecimal::add);
        System.out.println(result.divide(new BigDecimal(NUMBER_OF_ROUNDS), 2, RoundingMode.HALF_EVEN));
    }
}
