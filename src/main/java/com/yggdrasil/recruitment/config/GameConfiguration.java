package com.yggdrasil.recruitment.config;

import com.yggdrasil.recruitment.domain.Reward;

import java.math.BigDecimal;
import java.util.List;

public interface GameConfiguration {
    List<Reward> mainPhaseRewards();

    List<Reward> additionalRewards();

    List<Reward> additionalRewardsForSecondChance();

    BigDecimal getNumberOfAdditionalRewards();

    BigDecimal getNumberOfAdditionalRewardsInSecChanceGame();

}
