package com.yggdrasil.recruitment.config;

import com.yggdrasil.recruitment.domain.Reward;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.List;

import static com.yggdrasil.recruitment.domain.Reward.*;
import static com.yggdrasil.recruitment.domain.Reward.GAME_OVER;

public class EmbeddedGameConfiguration implements GameConfiguration {

    private static final List<Reward> BOXES = Arrays.asList(
            EURO_100, EURO_20, EURO_20, EURO_5, EURO_5, EURO_5,
            EURO_5, EURO_5, EXTRA_LIFE, GAME_OVER, GAME_OVER, GAME_OVER
    );

    private static final List<Reward> ADDITIONAL_REWARDS = Arrays.asList(
            Reward.EURO_5, Reward.EURO_10, Reward.EURO_20, Reward.SECOND_CHANCE
    );

    private static final List<Reward> ADDITIONAL_REWARDS_FOR_SECOND_CHANCE = Arrays.asList(
            Reward.EURO_5, Reward.EURO_10, Reward.EURO_20
    );


    @Override
    public List<Reward> mainPhaseRewards() {
        return BOXES;
    }

    @Override
    public List<Reward> additionalRewards() {
        return ADDITIONAL_REWARDS;
    }

    @Override
    public List<Reward> additionalRewardsForSecondChance() {
        return ADDITIONAL_REWARDS_FOR_SECOND_CHANCE;
    }

    @Override
    public BigDecimal getNumberOfAdditionalRewards() {
        return new BigDecimal(ADDITIONAL_REWARDS.size());
    }

    @Override
    public BigDecimal getNumberOfAdditionalRewardsInSecChanceGame() {
        return new BigDecimal(ADDITIONAL_REWARDS_FOR_SECOND_CHANCE.size());
    }
}
