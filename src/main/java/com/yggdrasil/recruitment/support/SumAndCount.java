package com.yggdrasil.recruitment.support;

import java.math.BigDecimal;
import java.math.RoundingMode;

public class SumAndCount {
    private BigDecimal sum = BigDecimal.ZERO;
    private int count = 0;

    public void add(BigDecimal toSumAndCount) {
        sum = sum.add(toSumAndCount);
        count++;
    }

    public BigDecimal average() {
        if (count == 0) {
            return BigDecimal.ZERO;
        } else {
            return sum.divide(new BigDecimal(count),2,  RoundingMode.HALF_EVEN);
        }
    }
}
