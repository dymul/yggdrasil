package com.yggdrasil.recruitment.domain;

import java.math.BigDecimal;
import java.util.Optional;

public enum Reward {

    EURO_100(new BigDecimal(100)){
        @Override
        public void addRewardTo(Game game) {
            game.increasePayoff(payoff);
        }
    },

    EURO_20(new BigDecimal(20)){
        @Override
        public void addRewardTo(Game game) {
            game.increasePayoff(payoff);
        }
    },

    EURO_10(new BigDecimal(10)){
        @Override
        public void addRewardTo(Game game) {
            game.increasePayoff(payoff);
        }
    },

    EURO_5(new BigDecimal(5)){
        @Override
        public void addRewardTo(Game game) {
            game.increasePayoff(payoff);
        }
    },

    EXTRA_LIFE(BigDecimal.ZERO){
        @Override
        public void addRewardTo(Game game) {
            game.extraLife();
        }
    },

    GAME_OVER(BigDecimal.ZERO){
        @Override
        public void addRewardTo(Game game) {
            game.gameOver();
        }
    },

    SECOND_CHANCE(BigDecimal.ZERO){
        @Override
        public void addRewardTo(Game game) {
            game.increasePayoff(game.secondChanceGame().flatMap(g -> Optional.of(g.play())).orElse(BigDecimal.ZERO));
        }
    },

    EMPTY(BigDecimal.ZERO){
        @Override
        public void addRewardTo(Game game) {
            //intentionally left blank
        }
    };

    protected final BigDecimal payoff;

    public BigDecimal getPayoff() {
        return payoff;
    }

    Reward(BigDecimal payoff) {
        this.payoff = payoff;
    }

    public abstract void addRewardTo(Game game);
}
