package com.yggdrasil.recruitment.domain;

import com.yggdrasil.recruitment.config.GameConfiguration;
import com.yggdrasil.recruitment.support.Permutations;
import lombok.RequiredArgsConstructor;

import java.util.*;
import java.util.function.Function;
import java.util.stream.Stream;

@RequiredArgsConstructor
public class AllGamesFactory {

    private final GameConfiguration config;

    private Function<List<Reward>, Game> boxesToGame = boxes -> new Game(boxes, Reward.EMPTY);

    public Stream<Game> allGames()  {
        return Permutations.of(config.mainPhaseRewards()).map(boxesToGame);
    }

}
