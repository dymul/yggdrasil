package com.yggdrasil.recruitment.domain;

import lombok.RequiredArgsConstructor;

import java.math.BigDecimal;
import java.util.Iterator;
import java.util.List;
import java.util.Optional;

@RequiredArgsConstructor
public class Game {

    private final List<Reward> boxes;
    private final Reward additionalReward;

    private int lives = 1;
    private BigDecimal payoff = BigDecimal.ZERO;

    public BigDecimal play() {
        Iterator<Reward> iterator = boxes.iterator();
        while (iterator.hasNext() && gameNotFinished()) {
            iterator.next().addRewardTo(this);
        }
        additionalReward.addRewardTo(this);
        return payoff;
    }

    public void increasePayoff(BigDecimal payoffToAdd) {
        payoff = payoff.add(payoffToAdd);
    }

    public void extraLife() {
        lives++;
    }
    public void gameOver() {
        lives--;
    }

    public Optional<Game> secondChanceGame() {
        return Optional.empty();
    }

    private boolean gameNotFinished() {
        return lives >= 1;
    }

}
