package com.yggdrasil.recruitment.domain;

import com.yggdrasil.recruitment.config.GameConfiguration;
import lombok.RequiredArgsConstructor;

import java.util.*;
import java.util.function.Supplier;
import java.util.stream.Stream;

@RequiredArgsConstructor
public class RandomGameFactory {

    private final Random random;
    private final GameConfiguration config;

    public Stream<Game> randomGameStream() {
        return Stream.generate(randomGameGenerator);
    }

    private Supplier<Game> randomGameGenerator = () -> {
        Reward additionalReward = getAdditionalRewardForGame();
        return additionalReward == Reward.SECOND_CHANCE ?
                gameWithSecondChance(additionalReward) : game(additionalReward);

    };

    private Reward getAdditionalRewardForGame() {
        return config.additionalRewards().get(random.nextInt(config.additionalRewards().size()));
    }

    private Reward getAdditionalRewardForSecondChanceGame() {
        return config.additionalRewardsForSecondChance().get(random.nextInt(config.additionalRewardsForSecondChance().size()));
    }


    private Game game(Reward additionalReward) {
        return new Game(randomBoxes(), additionalReward);
    }

    private Game gameWithSecondChance(Reward additionalReward) {
        return new GameWithSecondChance(randomBoxes(), additionalReward, secondChanceGame());
    }

    private Game secondChanceGame() {
        return game(getAdditionalRewardForSecondChanceGame());
    }

    private List<Reward> randomBoxes() {
        List<Reward> randomBoxes = new LinkedList<>(config.mainPhaseRewards());
        Collections.shuffle(randomBoxes, random);
        return randomBoxes;
    }


}
