package com.yggdrasil.recruitment.domain;

import java.util.List;
import java.util.Optional;

class GameWithSecondChance extends Game{

    private final Game secondChanceGame;

    GameWithSecondChance(List<Reward> boxes, Reward additionalReward, Game secondChanceGame) {
        super(boxes, additionalReward);
        if (secondChanceGame == null) {
            throw new IllegalArgumentException("secondChanceGame cannot be null");
        }
        this.secondChanceGame = secondChanceGame;
    }

    public Optional<Game> secondChanceGame() {
        return Optional.of(secondChanceGame);
    }

}
