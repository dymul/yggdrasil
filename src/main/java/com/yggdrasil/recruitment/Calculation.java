package com.yggdrasil.recruitment;

import com.yggdrasil.recruitment.config.GameConfiguration;
import com.yggdrasil.recruitment.domain.AllGamesFactory;
import com.yggdrasil.recruitment.domain.Game;
import com.yggdrasil.recruitment.config.EmbeddedGameConfiguration;
import com.yggdrasil.recruitment.support.SumAndCount;
import lombok.RequiredArgsConstructor;

import java.math.BigDecimal;
import java.math.RoundingMode;

import static com.yggdrasil.recruitment.domain.Reward.*;

@RequiredArgsConstructor
public class Calculation {

    private final AllGamesFactory gamesFactory;
    private final GameConfiguration gameConfiguration;

    BigDecimal calculate() {
        /*
        To calculate game expected value we need to calculate the first phase (when
        player randomly opens the boxes) expected value and additional reward expected value.
        Game expected value will be the sum of above
         */
        BigDecimal mainPhaseExpectedValue = calculateMainPhaseExpectedValue();
        BigDecimal additionalRewardExpectedValue = calculateAdditionalRewardExpectedValue(mainPhaseExpectedValue);
        return mainPhaseExpectedValue.add(additionalRewardExpectedValue);
    }

    private BigDecimal calculateMainPhaseExpectedValue() {
        /*
        To calculate main phase expected value we will assume that all opening orders are equally probable.
        The expected value will be the quotient of sum of the all games results and the number of games.
         */
        SumAndCount sumAndCount = new SumAndCount();
        gamesFactory.allGames()
                .map(Game::play)
                .forEach(sumAndCount::add);
        return sumAndCount.average();
    }

    private BigDecimal calculateAdditionalRewardExpectedValue(BigDecimal mainPhaseExpectedValue) {
        /*
        Calculation of additional reward is a bit more complicated - as the additional reward can be
        additional (second chance) game. Firstly we will calculate the expected value off additional reward
        assuming that additional reward is second chance game.

        As the second chance game consist of main phase game and additional reward (which cannot be another
        second chance game) the expected value is the sum of main phase game expected value and additional reward
        of second chance game expected value
         */
        BigDecimal secondChanceExpectedValue = mainPhaseExpectedValue.add(secondChanceAdditionalReward());

        /*
        As all additional reward are equally probable the additional reward expected value will be the sum
        of all options divided by number of options.
         */
        return EURO_5.getPayoff()
                .add(EURO_10.getPayoff())
                .add(EURO_20.getPayoff())
                .add(secondChanceExpectedValue)
                .divide(gameConfiguration.getNumberOfAdditionalRewards(), 2, RoundingMode.HALF_EVEN);
    }

    private BigDecimal secondChanceAdditionalReward() {
       /*
        As all additional reward are equally probable the additional reward expected value will be the sum
        of all options divided by number of options.
         */
        return EURO_5.getPayoff()
                .add(EURO_10.getPayoff())
                .add(EURO_20.getPayoff())
                .divide(gameConfiguration.getNumberOfAdditionalRewardsInSecChanceGame(), 2, RoundingMode.HALF_EVEN);
    }

    public static void main(String[] args) {
        GameConfiguration config = new EmbeddedGameConfiguration();
        Calculation c = new Calculation(new AllGamesFactory(config), config);

        System.out.println(c.calculate());
    }

}
